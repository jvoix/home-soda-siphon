# Home Soda Siphon - DIY

You love sparkling ("carbonated") water but hate to buy (and throw away) all these plastic bottles? You'd like to carbonate your tap water at home but don't want to buy equipment from a company with very questionable ethical practices? Here is a simply DIY solution using a 5 lb C02 tank, a pressure regulator, some fittings and a good looking siphon bottle!